CC=gcc
CFLAGS=-std=c11 -Wall -Wextra -Wpedantic -O0 -ggdb -Wvla -Werror
LDFLAGS=-lcunit
SOURCES_MAIN=main.c fun_list.c
SOURCES_TEST=fun_list.c ./test/test_fun_list.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=fun_list
TEST1=test1

all: $(EXECUTABLE) $(TEST1)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(CFLAGS) $(LDFLAGS) $(SOURCES_MAIN) -o $@

$(TEST1): $(OBJECTS) 
	$(CC) $(CFLAGS) $(LDFLAGS) $(SOURCES_TEST) -o $@

clean:
	@rm -f $(OBJECTS) $(EXECUTABLE) $(TEST1)

.PHONY:
	$(EXECUTABLE) $(TEST1) clean