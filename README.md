# Tutoring

### Mission

This project has the aim to help students with the concept of "lists". It's based on a basic course of programming in C. Enums and typedefs are excluded from the project.

### Retrieve the project

See the file [CONTRIBUTING.md](./CONTRIBUTING.md) that contains the list of steps to follow to clone and set-up the project

### Solution

In the branch `solution` there will my implementation for all the functions.