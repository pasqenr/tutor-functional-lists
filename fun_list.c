#include "fun_list.h"

/** PRIVATE **/

/** PUBLIC **/

struct node *node_create(int value)
{

}

void node_destroy(struct node *n)
{

}

struct node *list_hd(struct node *n)
{

}

struct node *list_tl(struct node *n)
{

}

int list_length(struct node *n)
{

}

int list_length_rec(struct node *n)
{

}

struct node *list_cons(struct node *n, struct node *x)
{

}

struct node *list_nth(struct node *n, int pos)
{

}

struct node *list_nth_rec(struct node *n, int pos)
{

}

struct node *list_rev(struct node *n)
{

}

struct node *list_rev_rec(struct node *n)
{

}

struct node *list_append(struct node *n1, struct node *n2)
{

}

struct node *list_append_rec(struct node *n1, struct node *n2)
{

}

struct node *list_concat(struct node **multi_list, int multi_list_len)
{

}

struct node *list_concat_rec(struct node **multi_list, int multi_list_len)
{

}

struct node *list_iter(struct node *n, void (*lambda)(struct node *n))
{

}

struct node *list_iter_rec(struct node *n, void (*lambda)(struct node *n))
{

}

struct node *list_iteri(struct node *n,
                        void (*lambda)(struct node *n, int idx))
{

}

struct node *list_iteri_rec(struct node *n,
                            void (*lambda)(struct node *n, int idx))
{

}

struct node *list_map(struct node *n,
                      struct node *(*lambda)(struct node *n))
{

}

struct node *list_map_rec(struct node *n,
                          struct node *(*lambda)(struct node *n))
{

}

int list_fold_left(struct node *n,
                   int value,
                   int (*lambda)(struct node *n, int value))
{

}

bool list_for_all(struct node *n, bool (*lambda)(struct node *n))
{

}

bool list_for_all_rec(struct node *n, bool (*lambda)(struct node *n))
{

}
