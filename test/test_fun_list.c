#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <stdbool.h>

#include "../fun_list.h"

void lambda1(struct node *n)
{
    n->value = n->value * 2;
}

void lambda2(struct node *n, int idx)
{
    n->value = n->value * idx;
}

struct node *lambda3(struct node *n)
{
    n->value = n->value * 2;

    return n;
}

int lambda4(struct node *n, int value)
{
    int ret = value;

    while (n) {
        ret += n->value;

        n = n->next;
    }

    return ret;
}

int lambda5(struct node *n, int value)
{
    return n->value * value;
}

bool lambda6(struct node *n)
{
    return n->value % 2 == 0;
}

bool lambda7(struct node *n)
{
    return n->value > 1;
}

int init_suite1(void)
{
    return 0;
}

int clean_suite1(void)
{
    return 0;
}

/**
 * Must define the guard!
 */
void test_define(void)
{
#ifndef FUN_LIST_H
    CU_ASSERT_TRUE(false)
#endif
}

/**
 * Test allocation1 and deallocation.
 */
void test_node_create_destroy(void)
{
    struct node *n1 = node_create(1);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_EQUAL(1, n1->value);
    CU_ASSERT_PTR_NULL(n1->next);

    node_destroy(n1);
}

void test_list_hd(void)
{
    struct node *n1 = node_create(1);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);

    CU_ASSERT_PTR_EQUAL(n1, list_hd(n1));
    CU_ASSERT_PTR_NULL(list_hd(NULL)); // Test NULL param

    node_destroy(n1);
}

void test_list_tl(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    CU_ASSERT_PTR_EQUAL(n2, list_tl(n1));
    CU_ASSERT_PTR_EQUAL(n3, list_tl(n2));
    CU_ASSERT_PTR_NULL(list_tl(n3));
    CU_ASSERT_PTR_NULL(list_tl(NULL)); // Test NULL param

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_length(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);
    struct node *saved = n1;

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    CU_ASSERT_EQUAL(3, list_length(n1));
    CU_ASSERT_EQUAL(2, list_length(n2));
    CU_ASSERT_EQUAL(1, list_length(n3));
    CU_ASSERT_EQUAL(0, list_length(NULL));
    CU_ASSERT_PTR_EQUAL(saved, n1);

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_length_rec(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);
    struct node *saved = n1;

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    CU_ASSERT_EQUAL(3, list_length_rec(n1));
    CU_ASSERT_EQUAL(2, list_length_rec(n2));
    CU_ASSERT_EQUAL(1, list_length_rec(n3));
    CU_ASSERT_EQUAL(0, list_length_rec(NULL));
    CU_ASSERT_PTR_EQUAL(saved, n1);

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_cons(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n2->next = n3;
    n1 = list_cons(n2, n1);

    CU_ASSERT_PTR_NOT_NULL(n1);
    CU_ASSERT_PTR_EQUAL(n2, n1->next);
    CU_ASSERT_PTR_EQUAL(n3, n1->next->next);
    CU_ASSERT_PTR_NULL(n1->next->next->next);

    CU_ASSERT_PTR_EQUAL(n1, list_cons(NULL, n1));
    CU_ASSERT_PTR_NULL(list_cons(NULL, NULL));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_nth(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    CU_ASSERT_PTR_EQUAL(n1, list_nth(n1, 0));
    CU_ASSERT_PTR_EQUAL(n2, list_nth(n1, 1));
    CU_ASSERT_PTR_EQUAL(n3, list_nth(n1, 2));
    CU_ASSERT_PTR_EQUAL(n3, list_nth(n2, 1)); // Start from middle

    CU_ASSERT_PTR_NULL(list_nth(n1, -1));
    CU_ASSERT_PTR_NULL(list_nth(n1, 10));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_nth_rec(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    CU_ASSERT_PTR_EQUAL(n1, list_nth_rec(n1, 0));
    CU_ASSERT_PTR_EQUAL(n2, list_nth_rec(n1, 1));
    CU_ASSERT_PTR_EQUAL(n3, list_nth_rec(n1, 2));
    CU_ASSERT_PTR_EQUAL(n3, list_nth_rec(n2, 1)); // Start from middle

    CU_ASSERT_PTR_NULL(list_nth_rec(n1, -1));
    CU_ASSERT_PTR_NULL(list_nth_rec(n1, 10));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_rev(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    struct node *r = list_rev(n1);

    CU_ASSERT_PTR_EQUAL(n3, r);
    CU_ASSERT_PTR_EQUAL(n2, r->next);
    CU_ASSERT_PTR_EQUAL(n1, r->next->next);
    CU_ASSERT_PTR_NULL(r->next->next->next);

    CU_ASSERT_PTR_NULL(list_rev(NULL));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_rev_rec(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    struct node *r = list_rev_rec(n1);

    CU_ASSERT_PTR_EQUAL(n3, r);
    CU_ASSERT_PTR_EQUAL(n2, r->next);
    CU_ASSERT_PTR_EQUAL(n1, r->next->next);
    CU_ASSERT_PTR_NULL(r->next->next->next);

    CU_ASSERT_PTR_NULL(list_rev_rec(NULL));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_iter(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    n1 = list_iter(n1, lambda1);

    CU_ASSERT_EQUAL(2, n1->value);
    CU_ASSERT_EQUAL(4, n2->value);
    CU_ASSERT_EQUAL(6, n3->value);

    CU_ASSERT_PTR_NULL(list_iter(NULL, lambda1));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_iter_rec(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    n1 = list_iter_rec(n1, lambda1);

    CU_ASSERT_EQUAL(2, n1->value);
    CU_ASSERT_EQUAL(4, n2->value);
    CU_ASSERT_EQUAL(6, n3->value);

    CU_ASSERT_PTR_NULL(list_iter_rec(NULL, lambda1));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_append(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);
    struct node *n4 = node_create(4);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n4);

    n1->next = n2;
    n3->next = n4;

    n1 = list_append(n1, n3);

    CU_ASSERT_PTR_EQUAL(n2, n1->next);
    CU_ASSERT_PTR_EQUAL(n3, n1->next->next);
    CU_ASSERT_PTR_EQUAL(n3, n2->next);
    CU_ASSERT_PTR_EQUAL(n4, n1->next->next->next);
    CU_ASSERT_PTR_EQUAL(n4, n2->next->next);
    CU_ASSERT_PTR_EQUAL(n4, n3->next);
    CU_ASSERT_PTR_NULL(n4->next);
    CU_ASSERT_PTR_NULL(n1->next->next->next->next);

    n1->next = n2;
    n3->next = n4;

    CU_ASSERT_PTR_EQUAL(n3, list_append(NULL, n3));
    CU_ASSERT_PTR_NULL(list_append(NULL, NULL));

    node_destroy(n4);
    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_append_rec(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);
    struct node *n4 = node_create(4);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n4);

    n1->next = n2;
    n3->next = n4;

    n1 = list_append_rec(n1, n3);

    CU_ASSERT_PTR_EQUAL(n2, n1->next);
    CU_ASSERT_PTR_EQUAL(n3, n1->next->next);
    CU_ASSERT_PTR_EQUAL(n3, n2->next);
    CU_ASSERT_PTR_EQUAL(n4, n1->next->next->next);
    CU_ASSERT_PTR_EQUAL(n4, n2->next->next);
    CU_ASSERT_PTR_EQUAL(n4, n3->next);
    CU_ASSERT_PTR_NULL(n4->next);
    CU_ASSERT_PTR_NULL(n1->next->next->next->next);

    n1->next = n2;
    n3->next = n4;

    CU_ASSERT_PTR_EQUAL(n3, list_append_rec(NULL, n3));
    CU_ASSERT_PTR_NULL(list_append_rec(NULL, NULL));

    node_destroy(n4);
    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_concat(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);
    struct node *n4 = node_create(4);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n4);

    n1->next = n2;
    n3->next = n4;
    struct node *multi_list[2] = { n1, n3 };

    struct node *res = list_concat(multi_list, 2);

    CU_ASSERT_PTR_EQUAL(n1, res);
    CU_ASSERT_PTR_EQUAL(n2, res->next);
    CU_ASSERT_PTR_EQUAL(n3, res->next->next);
    CU_ASSERT_PTR_EQUAL(n4, res->next->next->next);
    CU_ASSERT_PTR_NULL(res->next->next->next->next);

    CU_ASSERT_PTR_NULL(list_concat(NULL, 0));

    node_destroy(n4);
    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_concat_rec(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);
    struct node *n4 = node_create(4);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n4);

    n1->next = n2;
    n3->next = n4;
    struct node *multi_list[2] = { n1, n3 };

    struct node *res = list_concat_rec(multi_list, 2);

    CU_ASSERT_PTR_EQUAL(n1, res);
    CU_ASSERT_PTR_EQUAL(n2, res->next);
    CU_ASSERT_PTR_EQUAL(n3, res->next->next);
    CU_ASSERT_PTR_EQUAL(n4, res->next->next->next);
    CU_ASSERT_PTR_NULL(res->next->next->next->next);

    CU_ASSERT_PTR_NULL(list_concat_rec(NULL, 0));

    node_destroy(n4);
    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_iteri(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    n1 = list_iteri(n1, lambda2);

    CU_ASSERT_EQUAL(0, n1->value);
    CU_ASSERT_EQUAL(2, n2->value);
    CU_ASSERT_EQUAL(6, n3->value);

    CU_ASSERT_PTR_NULL(list_iteri(NULL, lambda2));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_iteri_rec(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    n1 = list_iteri_rec(n1, lambda2);

    CU_ASSERT_EQUAL(0, n1->value);
    CU_ASSERT_EQUAL(2, n2->value);
    CU_ASSERT_EQUAL(6, n3->value);

    CU_ASSERT_PTR_NULL(list_iteri_rec(NULL, lambda2));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_map(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    struct node *l = list_map(n1, lambda3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(l);

    CU_ASSERT_EQUAL(1, n1->value);
    CU_ASSERT_EQUAL(2, n2->value);
    CU_ASSERT_EQUAL(3, n3->value);

    CU_ASSERT_EQUAL(2, l->value);
    CU_ASSERT_EQUAL(4, l->next->value);
    CU_ASSERT_EQUAL(6, l->next->next->value);
    CU_ASSERT_PTR_NULL(l->next->next->next);

    CU_ASSERT_PTR_NULL(list_map(NULL, lambda3));

    node_destroy(l->next->next);
    node_destroy(l->next);
    node_destroy(l);
    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_map_rec(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    struct node *l = list_map_rec(n1, lambda3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(l);

    CU_ASSERT_EQUAL(1, n1->value);
    CU_ASSERT_EQUAL(2, n2->value);
    CU_ASSERT_EQUAL(3, n3->value);

    CU_ASSERT_EQUAL(2, l->value);
    CU_ASSERT_EQUAL(4, l->next->value);
    CU_ASSERT_EQUAL(6, l->next->next->value);
    CU_ASSERT_PTR_NULL(l->next->next->next);

    CU_ASSERT_PTR_NULL(list_map_rec(NULL, lambda3));

    node_destroy(l->next->next);
    node_destroy(l->next);
    node_destroy(l);
    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_fold_left(void)
{
    struct node *n1 = node_create(1);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(3);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    CU_ASSERT_EQUAL(15, list_fold_left(n1, 1, lambda4));
    CU_ASSERT_EQUAL(6, list_fold_left(n1, 1, lambda5));
    CU_ASSERT_EQUAL(1, list_fold_left(NULL, 1, lambda4));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_for_all(void)
{
    struct node *n1 = node_create(0);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(4);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    CU_ASSERT_TRUE(list_for_all(n1, lambda6));
    CU_ASSERT_TRUE(list_for_all(NULL, lambda6));
    CU_ASSERT_FALSE(list_for_all(n1, lambda7));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

void test_list_for_all_rec(void)
{
    struct node *n1 = node_create(0);
    struct node *n2 = node_create(2);
    struct node *n3 = node_create(4);

    CU_ASSERT_PTR_NOT_NULL_FATAL(n1);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n2);
    CU_ASSERT_PTR_NOT_NULL_FATAL(n3);

    n1->next = n2;
    n2->next = n3;

    CU_ASSERT_TRUE(list_for_all_rec(n1, lambda6));
    CU_ASSERT_TRUE(list_for_all_rec(NULL, lambda6));
    CU_ASSERT_FALSE(list_for_all_rec(n1, lambda7));

    node_destroy(n3);
    node_destroy(n2);
    node_destroy(n1);
}

int main(void)
{
    CU_pSuite pSuite = NULL;
    int failures = 0;

    if (CUE_SUCCESS != CU_initialize_registry()) {
        return CU_get_error();
    }

    /* add a suite to the registry */
    pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "test_define", test_define)) ||
        (NULL == CU_add_test(pSuite, "test_node_create_destroy", test_node_create_destroy)) ||
        (NULL == CU_add_test(pSuite, "test_list_hd", test_list_hd)) ||
        (NULL == CU_add_test(pSuite, "test_list_tl", test_list_tl)) ||
        (NULL == CU_add_test(pSuite, "test_list_length", test_list_length)) ||
        (NULL == CU_add_test(pSuite, "test_list_length_rec", test_list_length_rec)) ||
        (NULL == CU_add_test(pSuite, "test_list_cons", test_list_cons)) ||
        (NULL == CU_add_test(pSuite, "test_list_nth", test_list_nth)) ||
        (NULL == CU_add_test(pSuite, "test_list_nth_rec", test_list_nth_rec)) ||
        (NULL == CU_add_test(pSuite, "test_list_rev", test_list_rev)) ||
        (NULL == CU_add_test(pSuite, "test_list_rev_rec", test_list_rev_rec)) ||
        (NULL == CU_add_test(pSuite, "test_list_iter", test_list_iter)) ||
        (NULL == CU_add_test(pSuite, "test_list_iter_rec", test_list_iter_rec)) ||
        (NULL == CU_add_test(pSuite, "test_list_append", test_list_append)) ||
        (NULL == CU_add_test(pSuite, "test_list_append_rec", test_list_append_rec)) ||
        (NULL == CU_add_test(pSuite, "test_list_concat", test_list_concat)) ||
        (NULL == CU_add_test(pSuite, "test_list_concat_rec", test_list_concat_rec)) ||
        (NULL == CU_add_test(pSuite, "test_list_iteri", test_list_iteri)) ||
        (NULL == CU_add_test(pSuite, "test_list_iteri_rec", test_list_iteri_rec)) ||
        (NULL == CU_add_test(pSuite, "test_list_map", test_list_map)) ||
        (NULL == CU_add_test(pSuite, "test_list_map_rec", test_list_map_rec)) ||
        (NULL == CU_add_test(pSuite, "test_list_fold_left", test_list_fold_left))||
        (NULL == CU_add_test(pSuite, "test_list_for_all", test_list_for_all)) ||
        (NULL == CU_add_test(pSuite, "test_list_for_all_rec", test_list_for_all_rec))) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Run1 all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    failures = -1 * CU_get_number_of_failures();

    CU_cleanup_registry();

    return failures;
}
