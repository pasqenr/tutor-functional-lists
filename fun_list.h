/**
 * @file fun_list.h
 *
 * The concept of list is fully abstract in this implementation. So we keep
 * only the reference to the first element of our "list", that is, the head.
 */

/**
 * Include the needed headers for this exercise.
 */

/**
 * First, because this is a header file, it requires a "guard". Use the
 * standard POSIX naming convention. Use the preprocessor commands
 * "ifndef", "define", "endif".
 */

/**
 * In this exercise you must not use "enum" or "typedef". So you must use the
 * complete naming for structures.
 */

/**
 * Define a node structure with two fields:
 * - an integer named value;
 * - a pointer to a node named next.
 */

/**
 * \fn struct node *node_create(int value)
 * \brief Allocate and create a new node. The allocation must happen in the
 * heap. The pointer to the next node should be NULL because it doesn't point
 * to anything yet.
 *
 * \param value The value to assign to the new created node.
 *
 * \return The new heap-allocated node.
 */
struct node *node_create(int value);

/**
 * \fn void node_destroy(struct node *n)
 * \brief Deallocate and frees the node n.
 *
 * \param n The node to free.
 */
void node_destroy(struct node *n);

/**
 * \fn struct node *list_hd(struct node *n)
 * \brief Return the head, that is, the first element of the "list" of nodes.
 *
 * \param n A node or NULL.
 *
 * \return The first node or NULL if the "list" is empty.
 */
struct node *list_hd(struct node *n);

/**
 * \fn struct node *list_tl(struct node *n)
 * \brief Return the tail of the list. That is, the list without the head.
 *
 * \param n A node.
 *
 * \return The list without the head. If the param n is NULL then NULL is
 * returned.
*/
struct node *list_tl(struct node *n);

/**
 * \fn int list_length(struct node *n)
 * \brief Return the length of the list. That is, the number of nodes.
 *
 * \param n A node.
 *
 * \return The number of elements, 0 if the list is empty.
 */
int list_length(struct node *n);

/**
 * \fn int list_length_rec(struct node *n)
 * \brief Return the length of the list. That is, the number of nodes.
 * Recursive version.
 *
 * \param n A node.
 *
 * \return The number of elements, 0 if the list is empty.
 */
int list_length_rec(struct node *n);

/**
 * \fn struct node *list_cons(struct node *n, struct node *x)
 * \brief Insert x in front of the list. That is, x will become the new head.
 *
 * \param n The old head of the list.
 * \param x The element to insert in the front and so the new head also.
 *
 * \param Pointer to the new head of the list. If the inserted node is NULL then
 * the old list should be returned.
 */
struct node *list_cons(struct node *n, struct node *x);

/**
 * \fn struct node *list_nth(struct node *n, int pos)
 * \brief Return the node in position pos. Starting from 0.
 *
 * \param n A node.
 * \param pos The position of the node to retrieve.
 *
 * \return The node in position pos.
 * - if pos < 0 then NULL is returned;
 * - if pos > list_length(n) then NULL is returned.
 */
struct node *list_nth(struct node *n, int pos);

/**
 * \fn struct node *list_nth_rec(struct node *n, int pos)
 * \brief Return the node in position pos. Starting from 0.
 * Recursive version.
 *
 * \param n A node.
 * \param pos The position of the node to retrieve.
 *
 * \return The node in position pos.
 * - if pos < 0 then NULL is returned;
 * - if pos > list_length(n) then NULL is returned.
 */
struct node *list_nth_rec(struct node *n, int pos);

/**
 * \fn struct node *list_rev(struct node *n)
 * \brief Reverse the current list. That is, the elements in the list need to
 * be returned in the reversal order.
 *
 * \param n The head of the list.
 *
 * \return The reverted list. NULL if the list is empty.
 */
struct node *list_rev(struct node *n);

/**
 * \fn struct node *list_rev_rec(struct node *n)
 * \brief Reverse the current list. That is, the elements in the list need to
 * be returned in the reversal order.
 * Recursive version.
 *
 * \param n The head of the list.
 *
 * \return The reverted list. NULL if the list is empty.
 */
struct node *list_rev_rec(struct node *n);

/**
 * \fn struct node *list_append(struct node *n1, struct node *n2)
 * \brief Append the list n2 to the end of the list n1. That is, n1 should have
 * all its elements plus, after the previous "end", the elements of n2.
 *
 * \param n1 The first list.
 * \param n2 The second list.
 *
 * \return The list n1 with the elements of n2, added at the end.
 */
struct node *list_append(struct node *n1, struct node *n2);

/**
 * \fn struct node *list_append_rec(struct node *n1, struct node *n2)
 * \brief Append the list n2 to the end of the list n1. That is, n1 should have
 * all its elements plus, after the previous "end", the elements of n2.
 * Recursive version.
 *
 * \param n1 The first list.
 * \param n2 The second list.
 *
 * \return The list n1 with the elements of n2, added at the end.
 */
struct node *list_append_rec(struct node *n1, struct node *n2);

/**
 * \fn struct node *list_concat(struct node **multi_list, int multi_list_len)
 * \brief Create a new list that contains all the elements of the lists
 * contained in multi_list in that order.
 * Example:
 *      multi_list = { 1->2, 3->4 }
 * where 1->2 is a list with nodes (1) and (2). The result is:
 *      1->2->3->4
 * in particular, the function must return the pointer to the head, that is
 * the node (1) in that example.
 *
 * \param multi_list An array of lists.
 * \param multi_list_len The number of elements in the array multi_list.
 *
 * \return The head to the new flatten list. NULL if the array is empty.
 */
struct node *list_concat(struct node **multi_list, int multi_list_len);

/**
 * \fn struct node *list_concat_rec(struct node **multi_list, int multi_list_len)
 * \brief Create a new list that contains all the elements of the lists
 * contained in multi_list in that order.
 * Recursive version.
 *
 * Example:
 *      multi_list = { 1->2, 3->4 }
 * where 1->2 is a list with nodes (1) and (2). The result is:
 *      1->2->3->4
 * in particular, the function must return the pointer to the head, that is
 * the node (1) in that example.
 *
 * \param multi_list An array of lists.
 * \param multi_list_len The number of elements in the array multi_list.
 *
 * \return The head to the new flatten list. NULL if the array is empty.
 */
struct node *list_concat_rec(struct node **multi_list, int multi_list_len);

/**
 * \brief iter the function lambda to every node in order.
 *
 * \param n A node.
 * \param lambda The function to iter to every node in the list.
 *
 * \return The modified list.
 */
struct node *list_iter(struct node *n, void (*lambda)(struct node *n));

/**
 * \fn struct node *list_iter_rec(struct node *n, void (*lambda)(struct node *n))
 * \brief iter the function lambda to every node in order.
 * Recursive version.
 *
 * \param n A node.
 * \param lambda The function to iter to every node in the list.
 *
 * \return The modified list.
 */
struct node *list_iter_rec(struct node *n, void (*lambda)(struct node *n));

/**
 * \fn struct node *list_iteri(struct node *n,
                               void (*lambda)(struct node *n, int idx))
 * \brief Same as list_iter but the function lambda is called with a node
 * and it's index (position) in the list.
 *
 * \param n A node.
 * \param lambda The function to apply at every node and it's index in the
 * list.
 *
 * \return The modified list.
 */
struct node *list_iteri(struct node *n,
                        void (*lambda)(struct node *n, int idx));

/**
 * \fn struct node *list_iteri(struct node *n,
                               void (*lambda)(struct node *n, int idx))
 * \brief Same as list_iter_rec but the function lambda is called with a node
 * and it's index (position) in the list.
 * Recursive version.
 *
 * \param n A node.
 * \param lambda The function to apply at every node and it's index in the
 * list.
 *
 * \return The modified list.
 */
struct node *list_iteri_rec(struct node *n,
                            void (*lambda)(struct node *n, int idx));


/**
 * \fn struct node *list_map(struct node *n,
                             struct node *(*lambda)(struct node *n))
 * \brief Applies the function lambda to every node of n and return a new
 * list with the new nodes. The original list must be untouched.
 *
 * \param n The list.
 * \param lambda The function to apply to every node to the new list.
 *
 * \return The new list with the application of lambda to all the old nodes.
 */
struct node *list_map(struct node *n,
                      struct node *(*lambda)(struct node *n));

/**
 * \fn struct node *list_map_rec(struct node *n,
                                 struct node *(*lambda)(struct node *n))
 * \brief Applies the function lambda to every node of n and return a new
 * list with the new nodes. The original list must be untouched.
 * Recursive version.
 *
 * \param n The list.
 * \param lambda The function to apply to every node to the new list.
 *
 * \return The new list with the application of lambda to all the old nodes.
 */
struct node *list_map_rec(struct node *n,
                          struct node *(*lambda)(struct node *n));

/**
 * \fn list_fold_left(struct node *n,
 *                 int value,
 *                 int (*lambda)(struct node *n, int value))
 *
 * \brief Applies the function lambda to every node, starting from the first,
 * with the value updated with every lambda call.
 * That is:
 *   list_fold_left(n1, value, lambda) = .. lambda (n2, lambda(n1, value)) ..
 *
 * \param n The list.
 * \param value The starting value that is used to compute lambda.
 * \param lambda The function to apply to every node in order with
 * the value updated each call.
 *
 * \return The value of the last application of lambda. If n is NULL
 * then value is returned.
 */
int list_fold_left(struct node *n,
                   int value,
                   int (*lambda)(struct node *n, int value));

/**
 * \fn bool for_all(struct node *n, bool (*prop)(struct node *n))
 *
 * \brief Checks if all the elements satisfy the predicate lambda.
 * That is, the function must return:
 *      lambda(n1) && lambda(n2) && ...
 *
 * \param n The first node of the list.
 * \param lambda Checks the user predicate on the current node n.
 *
 * \return True if all the nodes satisfy the predicate expressed from
 * the function lambda. False otherwise.
 * Follow from logic, if there aren't elements in the list then the predicate
 * is true.
 */
bool list_for_all(struct node *n, bool (*lambda)(struct node *n));

/**
 * \fn bool for_all(struct node *n, bool (*prop)(struct node *n))
 *
 * \brief Checks if all the elements satisfy the predicate lambda.
 * That is, the function must return:
 *      lambda(n1) && lambda(n2) && ...
 * Recursive version.
 *
 * \param n The first node of the list.
 * \param lambda Checks the user predicate on the current node n.
 *
 * \return True if all the nodes satisfy the predicate expressed from
 * the function lambda. False otherwise.
 * Follow from logic, if there aren't elements in the list then the predicate
 * is true.
 */
bool list_for_all_rec(struct node *n, bool (*lambda)(struct node *n));
