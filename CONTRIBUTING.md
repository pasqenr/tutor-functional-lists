### General information

The project is written in C, standard C11. To build the executable and the tests we use the [Meson build system](https://mesonbuild.com/).

### Style Guide

Try to keep the code clean. Prefer intermediate variables when needed.
Take a look at [Linux Kernel Coding Style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html).
The project has a [EditorConfig](https://editorconfig.org/) file. If your editor supports it you're welcom to install the extension.

Always test your program before push changes.

# Install Guide

### CUnit
```bash
sudo apt install libcunit1 libcunit1-doc libcunit1-dev
```

### Pip (+ Python 3)
```bash
sudo apt install python3-pip
```

### Ninja
```bash
sudo pip3 install ninja
```

### Meson
```bash
sudo pip3 install meson
```

### SSH Key
See [SSH Keys on GitLab](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html).

### Git account
```bash
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

## Usage

### Fork & Clone
Start with forking the project to your account and then clone it.

```bash
git clone git@gitlab.com:<your-username>/tutor-functional-lists.git
cd tutor-functional-lists
```

### Build Makefile
This is the simplest way to build the project.

```bash
make

# Optionally, to remove the compiled files
make clean
```

### Build meson
```bash
mkdir builddir
meson builddir/
```

### Build executable and tests
```bash
cd builddir
ninja
```

The main executable is *auc*. The tests are named as *test1*, *test2*, etc. It's better to execute the tests from the command line but Meson (ninja) can do that automatically:
```bash
ninja test

# In alternative, to get the direct output
./test1
```
